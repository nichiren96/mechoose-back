const express = require("express");
const router = express.Router();

const candidateController = require("../controllers/candidates");

router.route("/").post(candidateController.createCandidate);

module.exports = router;
