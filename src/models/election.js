const mongoose = require("mongoose");

const electionSchema = new mongoose.Schema({
  label: {
    type: String,
    required: [true, "Veuillez renseigner un titre pour cette election"],
  },
  status: {
    type: String,
    enum: ["ongoing", "closed"],
  },
  numberOfCandidates: {
    type: Number,
    required: [true, "Veuillez renseigner le nombre de candidats"],
  },
});

const Election = mongoose.model("Election", electionSchema);

module.exports = Election;
