const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Veuillez renseigner votre nom"],
  },
  email: {
    type: String,
    required: [true, "Veuillez renseigner votre email"],
    unique: true,
    validate: [validator.isEmail, "Veuillez renseigner un email valide"],
  },
  role: {
    type: String,
    enum: ["user", "admin"],
  },
  password: {
    type: String,
    required: [true, "Veuillez renseigner un mot de passe"],
    minlength: 8,
    select: false,
  },
});

userSchema.pre("save", async function (next) {
  this.password = await bcrypt.hash(this.password, 12);
  next();
});

userSchema.methods.isPasswordCorrect = async function (
  candidatePassword,
  userPassword
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

const User = mongoose.model("User", userSchema);

module.exports = User;
