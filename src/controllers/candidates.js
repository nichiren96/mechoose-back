const catchAsync = require("../utils/catchAsync");
const Candidate = require("../models/candidate");

exports.createCandidate = catchAsync(async (req, res, next) => {
  const newCandidate = await Candidate.create(req.body);

  res.status(201).json({
    status: "success",
    data: newCandidate,
  });
});
